"use client"
import React from 'react'
import styles from "./Navbar.module.scss"
import Image from 'next/image'
import LogoCompany from '../../../static/Images/companylogo.jpg'
// import LogoCompany "../../../static/images/companylogo.jpg"

export const Navbar = () => {
    return (
        <div>
            <div className={styles.navbar_container}>
                <div>
                    {/* <Image
                src={LogoCompany}
                width={100}
                height={30}
                alt="Picture of the author"
            /> */}
                    Icono De compania
                </div>
                <div className={styles.navbar_item_text}>
                    <span>Features</span>
                    <span>Testimonials</span>
                    <span>Hightlights</span>
                    <span>Pricing</span>
                    <span>FAQ</span>
                </div>
            </div>
        </div>
    )
}
