import React from 'react'
import styles from './PrincipalSection.module.scss'
// import styles "./PrincipalSection.module.scss"
export const PrincipalSection = () => {
    return (
        <div className={styles.container_principal_section}>
            <div>
                <span className={styles.title_principal_section} style={{display:"flex",justifyContent:"center",alignItems:"center"}}>Lorem Ipsum</span> <br/>
                <span className={styles.subtitle_principal_section} style={{justifyContent:"center",display:"flex",alignItems:"center"}}>Explore our cutting-edge dashboard, delivering high-quality solutions tailored to your needs. <br/>
                    Elevate your experience with top-tier features and services.</span>
            </div>
        </div>
    )
}
